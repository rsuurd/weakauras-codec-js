const pako = require('pako');

const SUPPORTED_CHARACTERS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789()';

const decode = function(string) {
    const bytes = [];

    const sanitized = string.trim().replace(/!/, '');

    if (/^[a-zA-Z0-9()]*$/.test(sanitized)) {
      for (var i = 0; i < sanitized.length; i += 4) {
          const field = readInt(sanitized, i);

          bytes.push(field & 255);
          bytes.push((field >> 8) & 255);
          bytes.push(field >> 16);
      }
    }

    return bytes;
}

const readInt = function(string, index) {
  const a = readByte(string, index);
  const b = readByte(string, index + 1);
  const c = readByte(string, index + 2);
  const d = readByte(string, index + 3);

  return (d << 18) + (c << 12) + (b << 6) + a;
}

const readByte = function(string, index) {
  return (index < string.length) ? SUPPORTED_CHARACTERS.indexOf(string.charAt(index)) : 0;
}

const deserialize = function(string) {
    const iterator = createIterator(string);

    if (iterator.next().value === '^1') {
      return deserializeValue(iterator.next().value, iterator);
    } else {
      throw "Not Ace-serialized content";
    }
}

const createIterator = function(string) {
  let current = 0;
  const matches = string.match(/(\^.)([^^]*)/g);

  return {
    next: function() {
      return {
        value: matches[current++],
        done: matches[current] === '^^'
      };
    }
  }
}

const deserializeValue = function(string, iterator) {
    const control = string.substr(0, 2);
    const data = string.substr(2);

    switch(control) {
        case '^S':
            return deserializeString(data);
        case '^N':
            return Number(data);
        case '^F':
            throw "Floats not supported yet";
        case '^B':
            return true;
        case '^b':
            return false;
        case '^T':
            return deserializeObject(iterator);
        case '^Z':
        default:
            return null;
    }
}

const deserializeString = function(string) {
    return string.replace(/~./g, function(match) {
      const escape = match.charCodeAt(1);
      let replacement;

      if (escape < 0x7a) {
        replacement = escape - 64;
      } else if (escape == 0x7a) {
          replacement = 0x1e;
      } else if (escape == 0x7b) {
          replacement = 0x7f;
      } else if (escape == 0x7c) {
          replacement = 0x7e;
      } else if (escape == 0x7d) {
          replacement = 0x5e;
      }

      if (replacement === undefined) {
          throw `Unknown escape char: '${match}'`;
      }

      return String.fromCharCode(replacement);
    });
}

const deserializeObject = function(iterator) {
    let result = iterator.next();


    const object = (result.value.substr(0, 2) === '^N') ? [] : {};

    while(!result.done) {
      const element = result.value;

      if (element.substr(0, 2) === '^t') {
          break;
      } else {
          const key = deserializeValue(element, iterator);

          result = iterator.next();

          const value = deserializeValue(result.value, iterator);

          if (typeof key === 'number') {
            object[key - 1] = value;
          } else {
            object[key] = value;
          }
      }

      result = iterator.next();
    }

    return object;
}

module.exports = {
    import: function(string) {
        const decoded = decode(string);
        const deflated = pako.ungzip(decoded, { raw: true, to: 'string' });

        return deserialize(deflated);
    }
};
