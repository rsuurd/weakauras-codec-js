const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'weakauras-codec.js',
    library: 'weakaurasCodec',
    libraryTarget: 'var',
  },
};
